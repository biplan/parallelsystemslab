#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include <boost/phoenix/bind/bind_member_function.hpp>

#include <iostream>
#include "ThreadPool.h"
#include "Converter.h"
#include "ConcatFiles.h"
using namespace std;
using namespace boost;


ThreadPool::ThreadPool(int cnt)
{
	/*boost::asio::io_service::work work(ioService);
	for (int i=0; i<cnt; ++i)
		tgroup.create_thread(
			boost::bind(&boost::asio::io_service::run, &ioService)
		);
	ioService.run();*/
	
}

void ThreadPool::test(Converter conv)
{
	int a = 10;
}

void ThreadPool::AddToGroup(Converter& conv)
{
	interprocess::scoped_lock<mutex> locker(lock);
	//ioService.post(boost::bind(&ThreadPool::test, this, conv));
	tgroup.create_thread([&conv]() {conv.Convert();});
	
	//ioService.post(boost::bind([&conv]() {conv.Convert();}));
//	ioService.post(boost::bind(pointer, conv->GetConcatFile(), conv->GetStartPos(), conv->GetFileText()));
	
}

void ThreadPool::JoinAll()
{
	//ioService.stop();
	tgroup.join_all();
}

ThreadPool::~ThreadPool()
{
	tgroup.join_all();
}
