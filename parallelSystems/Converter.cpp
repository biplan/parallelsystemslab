#include "Converter.h"
#include <string>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include "ConcatFiles.h"
#include <iostream>
using namespace std;
using namespace boost;

Converter::Converter()
{
	inputPath = "";
	startPos = 0;
	fileLength = 0;
	fileText = "";
}
Converter::Converter(string path)
{
	inputPath = "";
	startPos = 0;
	fileLength = 0;
	fileText = "";
	if (filesystem::exists(path))
		inputPath = path;
}


Converter::~Converter()
{
}

void Converter::Load()
{
	if (inputPath != "")
	{
		filesystem::ifstream file(inputPath);
		string text = "";
		long long int length = 0;
		file.seekg(0, filesystem::ifstream::end);
		length = file.tellg();
		file.seekg(0, filesystem::ifstream::beg);
		char* inputTextBuffer = new char[length];
		file.read(inputTextBuffer, length);
		string inputText(inputTextBuffer);
		fileChar = inputTextBuffer[0];
		fileText = inputText;
		fileLength = length;
		file.close();
	}
}

void Converter::Convert()
{
	to_upper(fileText);
	output->WriteToFile(startPos, fileText);
}

void Converter::SetStartPos(long long int pos)
{
	startPos = pos;
}

long long int Converter::GetLength()
{
	return fileLength;
}

char Converter::GetFileChar()
{
	return fileChar;
}

string Converter::GetFileText()
{
	return fileText;
}

void Converter::SetConcatFile(ConcatFiles* concatFile)
{
	output = concatFile;
}