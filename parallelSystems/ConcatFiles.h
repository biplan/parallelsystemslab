#pragma once
#include <boost\filesystem.hpp>
#include <boost\thread\mutex.hpp>
#include <string>
using namespace std;
using namespace boost;
class ConcatFiles
{
public:
	ConcatFiles(string fileName);
	void WriteToFile(long long int startPos, string fileText);
	void WriteWhiteSpace(int whiteSpaceLength);
	virtual ~ConcatFiles();
private:
	filesystem::ofstream file;
	mutex lock;
};

