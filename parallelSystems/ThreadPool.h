#pragma once
#include <boost\asio\io_service.hpp>
#include <boost\bind.hpp>
#include <boost\thread.hpp>
#include <boost\thread\mutex.hpp>
#include <boost\interprocess\sync\scoped_lock.hpp>
#include "Converter.h"

using namespace std;
using namespace boost;
class ThreadPool
{
public:
	ThreadPool(int cnt);
	void test(Converter conv);
	void AddToGroup(Converter& conv);
	void JoinAll();
	~ThreadPool();
private:
	void Convert(int a);
	thread_group tgroup;
	mutex lock;
	asio::io_service ioService;
};

