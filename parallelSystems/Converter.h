#pragma once
#include <string>
#include "ConcatFiles.h"
using namespace std;
class Converter
{
public:
	Converter();
	Converter(string path);
	virtual ~Converter();
	void Load();
	void Convert();
	void SetStartPos(long long int pos);
	long long int GetLength();
	char GetFileChar();
	string GetFileText();
	void SetConcatFile(ConcatFiles* concatFile);
private:
	string inputPath;
	char fileChar;
	string fileText;
	long long int fileLength;
	long long int startPos;
	ConcatFiles* output;

};

