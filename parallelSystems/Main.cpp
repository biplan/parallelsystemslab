#include <iostream>
#include <string>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <map>
#include <vector>

#include "Converter.h"
#include "ThreadPool.h"

using namespace std;
using namespace boost;



void CreateFiles(int sizeMin=2000000, int sizeMax=10000000)
{
	srand(time(0));
	filesystem::create_directory("in");
	for (char c = 'a'; c <= 'z'; c++)
	{
		string fileName({c});
		fileName += ".txt";
		// = toString
		filesystem::ofstream file("in\\"+fileName);
		//file << fileName;
		int size = rand() % (sizeMax - sizeMin) + sizeMin;
		for (int i = 0; i < size; i++)
			file << c;
		file.close();
	}
	cout << "Files Ready!" << endl;
}

vector<Converter> LoadFiles()
{
	vector<Converter> convertersArray = vector<Converter>();
	filesystem::directory_iterator end_itr;
	for (filesystem::directory_iterator itr("in"); itr != end_itr; itr++)
	{
		if (filesystem::is_regular_file(itr->path()))
		{
			long long int size = filesystem::file_size(itr->path());
			Converter converter = Converter(itr->path().string());
			converter.Load();
			convertersArray.push_back(converter);
			converter.~Converter();
//			Converter c = Converter(itr->path().string());
//			c.LoadAndConvert();
//			c.~Converter();

		}
	}
	return convertersArray;
}

void MergeFiles(vector<Converter> inputConverters)
{
	ConcatFiles concater("out.txt");
	map<char, pair<long long int, long long int>> letters;
	long long int sizeOfFile = 0;
	for (int i = 0; i < inputConverters.size(); i++)
	{
		inputConverters[i].SetConcatFile(&concater);
		letters[inputConverters[i].GetFileChar()] = { i, inputConverters[i].GetLength() };
		sizeOfFile += inputConverters[i].GetLength();
	}
	//concater.WriteWhiteSpace(sizeOfFile);
	//thread_group tgroup;
	ThreadPool tPool(20);
	long long int startPos = 0;
	for (auto& item : letters)
	{
		auto& pair = item.second;
		auto con = inputConverters[pair.first];
		Converter& converter = inputConverters[pair.first];
		converter.SetStartPos(startPos);
		//con.SetStartPos(startPos);
		tPool.AddToGroup(converter);
		//tgroup.create_thread([&converter]() {converter.Convert();});
		startPos += pair.second;
	}
	//tgroup.join_all();
	tPool.JoinAll();
}

bool CheckConcatFile()
{
	filesystem::ifstream file("out.txt");
	char c;
	char last = 'A';
	long long int length = file.tellg();
	file.seekg(0, filesystem::ifstream::beg);
	char* inputTextBuffer = new char[length];
	file.read(inputTextBuffer, length);
	for (int i = 0; i < length; ++i)
	{
		if (inputTextBuffer[i] >= 'A' && inputTextBuffer[i] <= 'Z')
			if (last > inputTextBuffer[i])
			{
				file.close();
				return false;
			}
			else
				last = inputTextBuffer[i];
	}
	file.close();
	return true;
}

int main(int argc, char**argv) 
{
	int minFileSize = 1000;
	int maxFileSize = 5000;
	int behavior = 1;
	if (behavior == 1)
		CreateFiles(minFileSize, maxFileSize);
	
	MergeFiles(LoadFiles());
	cout << "CheckOutFile " << CheckConcatFile() << endl << "Press any key to exit" << endl;
	cin.get();
	return 0;
}