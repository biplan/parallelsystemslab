#include "ConcatFiles.h"
#include <boost\filesystem.hpp>
#include <boost\thread\mutex.hpp>
#include <boost\interprocess\sync\scoped_lock.hpp>
#include <iostream>
#include <string>
using namespace std;
using namespace boost;

ConcatFiles::ConcatFiles(string fileName)
{
		file.open(fileName);
}

void ConcatFiles::WriteToFile(long long int startPos, string fileText)
{
	interprocess::scoped_lock<mutex> locker(lock);
	file.seekp(startPos);
	file << fileText;
	cout << "Writen to file from " << startPos << endl;
	//fseek(file, offset, SEEK_SET);
	//fwrite(&array[0], sizeof(char), array.size(), file);
	//printf("Written '%c' from %d to %d\n", array[0], offset, offset + array.size());
}

void ConcatFiles::WriteWhiteSpace(int whiteSpaceLength)
{
	interprocess::scoped_lock<mutex> locker(lock);
	file.seekp(0);
	string out = "";
	for (int i = 0; i < whiteSpaceLength; i++)
		out += " ";
	file << out;
	
}

ConcatFiles::~ConcatFiles()
{
	file.close();
}
